import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EAppFrameComponent } from './components/e-app-frame/e-app-frame.component';

const routes: Routes = [
  {path:'newPageApp', component: EAppFrameComponent},
  {path:'', redirectTo:'newPageApp', pathMatch:'full'},
];

@ NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
