import { Injectable } from '@angular/core';
import { IDataCard, ISMLinks } from '../interfaces/generalInterface';
import { Observable } from 'rxjs';

@ Injectable({
  providedIn: 'root'
})
export class DataService {
cardData : IDataCard[];
socialmedialinks: ISMLinks[];
//lets assume socialMediaLinks are comming from a http call instead of hard coding here.
//so we use observable 
socialMediaData : Observable<ISMLinks[]>;

  constructor() {
  this .cardData = [
    {
      title:"Fichipizza",
      hasSubHeadings: false,
      subHeadingText: "",
      sideHeadingText:"Our Unique Culinary Creations",
      sideHeadingOrientaion:"right",
      descriptionText:"A unique combination of Mediterranean and Italian pizza, topped with Mascarphone, proscuitto, figs, grando panado, honey and arugula.",
      additionalText:"Some more Text"
    },
    {
      title:"Weekend Grand Buffet",
      hasSubHeadings: false,
      subHeadingText: "",
      sideHeadingText:"This Month's Promotions",
      sideHeadingOrientaion:"left",
      descriptionText:"Featuring mouthwatering combinations with a choice of five different salads, six enticing appetizers, six main entrees and five choicest desserts. Free flowing bubbly and soft drinks. All for just $19.99 per person"

    },
    {
      title:"Tommy McHugh",
      hasSubHeadings: true,
      subHeadingText: "Executive Chef",
      sideHeadingText:"Meet our Culinary Specialists",
      sideHeadingOrientaion:"right",
      descriptionText:"Award winning three-star Michelin chef with wide International experience having worked closely with whos-who in the culinary world, he specializes in creating mouthwatering Mediterranean-Italian experiences.",
      additionalText:"Some additional Text"
    }
  ];

//Assume this is coming from http call

this.socialmedialinks=[
  {
  linktext:"Google",
  url:"http://google.com/+",
  image:"fab fa-google-plus-g"
},
  {
  linktext:"Facebook",
  url:"http://www.facebook.com/profile.php?id=",
  image:"fab fa-facebook"
},
  {
  linktext:"LinkedIn",
  url:"http://www.linkedin.com/in/",
  image:"fab fa-linkedin"
},
{
linktext:"Twitter",
url:"http://twitter.com/",
image:"fab fa-twitter-square"
},
{
linktext:"Youtube",
url:"http://youtube.com/",
image:"fab fa-youtube-square"
},
{
linktext:"Contact",
url:"mailto:",
image:"far fa-envelope"
}
];
}

    getcardDetails(): IDataCard[] {
   return this .cardData;
    }

    getSMLinks(){
      this.socialMediaData = new Observable(observer =>{
        //delay of 1 second to see visually that the data is coming from this subscription
        setTimeout(()=>{
          observer.next(this.socialmedialinks);
          console.log("Data Passed from the Data Service Observable");
        },1000);
      })
      return this.socialMediaData;
    }
}
