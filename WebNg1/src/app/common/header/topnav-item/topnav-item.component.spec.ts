import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopnavItemComponent } from './topnav-item.component';

describe('TopnavItemComponent', () => {
  let component: TopnavItemComponent;
  let fixture: ComponentFixture<TopnavItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopnavItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopnavItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
