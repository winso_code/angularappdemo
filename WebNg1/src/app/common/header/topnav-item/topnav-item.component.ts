import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

@ Component({
  selector: 'app-topnav-item',
  templateUrl: './topnav-item.component.html',
  styleUrls: ['./topnav-item.component.css']
})
export class TopnavItemComponent implements OnInit {
 public topNavList: {}[];
  constructor() { }

  ngOnInit() {
    this .topNavList =[
    {
      title:"Home",
      hrefLink:"#",
      hasSubmenu: false,
      iconClass:"fas fa-home"
    },
    {
      title:"About",
      hrefLink:"#",
      hasSubmenu: false,
      iconClass:"fas fa-info-circle"
    },
    {
      title:"Menu",
      hrefLink:"#",
      hasSubmenu: true,
      submenuItems:[{
        title:"Submenu1",
        iconClass:""
      },
      {
        title:"Submenu2",
        iconClass:"",
        hrefLink:""
      },
      {
        title:"Submenu3",
        iconClass:"",
        hrefLink:""
      }
    ]

    },
    {
      title:"Contact",
      hrefLink:"", hasSubmenu: false
    }
  ];
  }

}
