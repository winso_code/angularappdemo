import { Component, OnInit } from '@angular/core';

@ Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
 public imgUrl:string;
  constructor() { }

  ngOnInit() {
    this .imgUrl = "assets/logo/logo1.png";
  }

}
