import { Component, OnInit } from '@angular/core';
import { IFooterLinks, ISMLinks } from '../../interfaces/generalInterface';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  footerlinks:IFooterLinks[];
  socialmedialinks:ISMLinks[];
  constructor(private _dataService : DataService) { }

  ngOnInit() {
      this.footerlinks=[
        {
        linktext:"Home",
        url:""
      },
        {
        linktext:"About",
        url:""
      },
        {
        linktext:"Menu",
        url:""
      },
        {
        linktext:"Contact",
        url:""
      }
      ];
    //subscription to dataService
    this._dataService.getSMLinks().subscribe(
      data =>{
        this.socialmedialinks = data;
      }
    );
  }

}
