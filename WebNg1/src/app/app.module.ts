import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LeftNavComponent } from './common/left-nav/left-nav.component';
import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { ProgressBarComponent } from './common/progress-bar/progress-bar.component';
import { TopnavItemComponent } from './common/header/topnav-item/topnav-item.component';
import { EAppFrameComponent } from './components/e-app-frame/e-app-frame.component';
import { MainTopBodyComponent } from './components/section-one/section.one.component';
import { BottomBodyComponent } from './components/section-two/section-two.component';
import { CustomCardComponent } from './components/custom-card/custom-card.component';
import { Observable } from 'rxjs';

@ NgModule({
  declarations: [
    AppComponent,
    LeftNavComponent,
    HeaderComponent,
    FooterComponent,
    ProgressBarComponent,
    TopnavItemComponent,
    EAppFrameComponent,
    MainTopBodyComponent,
    BottomBodyComponent,
    CustomCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
