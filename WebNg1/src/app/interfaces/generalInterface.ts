export interface IDataCard {
    title:string,
    hasSubHeadings: boolean,
    subHeadingText?: string,
    sideHeadingText: string,
    sideHeadingOrientaion:string,
    descriptionText: string,
    additionalText?:string
}

export interface IFooterLinks{
        linktext:string,
        url:string
}

export interface ISMLinks{
    linktext:string,
    url:string,
    image?:string
}
