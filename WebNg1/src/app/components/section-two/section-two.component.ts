import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { IDataCard } from '../../interfaces/generalInterface';

@ Component({
  selector: 'app-section-two',
  templateUrl: './section-two.component.html',
  styleUrls: ['./section-two.component.css']
})
export class BottomBodyComponent implements OnInit {

cardDetails: IDataCard[];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    //get data from the DataService
    this .cardDetails = this .dataService.getcardDetails();
    //In the html file this is also being passed to the child component (custom card component)
  }

}
