import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EAppFrameComponent } from './e-app-frame.component';

describe('EAppFrameComponent', () => {
  let component: EAppFrameComponent;
  let fixture: ComponentFixture<EAppFrameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EAppFrameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EAppFrameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
