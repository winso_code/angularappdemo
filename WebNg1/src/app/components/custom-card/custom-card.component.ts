import { Component, OnInit, Input } from '@angular/core';
import { IDataCard } from '../../interfaces/generalInterface';
import { DataService } from '../../services/data.service';

@ Component({
  selector: 'app-custom-card',
  templateUrl: './custom-card.component.html',
  styleUrls: ['./custom-card.component.css']
})
export class CustomCardComponent implements OnInit {
  showMoreText:boolean;
  //value passed from bottom-body component via DataService
  @Input() cardData : IDataCard[];
  constructor() { }

  ngOnInit() {

  }

  ToggleMoreText(){
    // if(flag){
      this.showMoreText = !this.showMoreText;
    // }
  }
}
